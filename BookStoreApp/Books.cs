﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace BookStoreApp
{
    public partial class Books : Form
    {
        public Books()
        {
            InitializeComponent();
            populate();
        }

        private void label2_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
        SqlConnection Con =new SqlConnection(@"");
        private void populate()
        {
            Con.Open();
            string query = "select  * from BookTbl";
            SqlDataAdapter sda=new SqlDataAdapter(query,Con);   
            SqlCommandBuilder builder = new SqlCommandBuilder(sda);    
            var ds = new DataSet();
            sda.Fill(ds);  
            BookDgv.DataSource = ds.Tables[0];   
            Con.Close();
        }
        private void Filter()
        {
            Con.Open();
            string query = "select  * from BookTbl where BCat = '"+ CatCbSearchCb.SelectedItem.ToString()+"'";
            SqlDataAdapter sda = new SqlDataAdapter(query, Con);
            SqlCommandBuilder builder = new SqlCommandBuilder(sda);
            var ds = new DataSet();
            sda.Fill(ds);
            BookDgv.DataSource = ds.Tables[0];
            Con.Close();
        }
        private void SaveBtn_Click(object sender, EventArgs e)
        {
            if (BnameTb.Text == "" || BAuthorTb.Text == "" || QtyTb.Text == "" || PriceTb.Text == "" || BcatCbo.SelectedIndex == -1)
            {
                MessageBox.Show("Điền thiếu thông tin");
            }
            else
            {
                try
                {
                    Con.Open();
                    string query = "Insert into BookTB1 values('" + BnameTb.Text + "','" + BAuthorTb.Text + "','"+BcatCbo.SelectedItem.ToString()+"',"+QtyTb.Text+","+PriceTb.Text+")"
                     SqlCommand cmd =new SqlCommand(query, Con);
                    cmd.ExecuteNonQuery();
                    MessageBox.Show("Sách đã được lưu thành công");
                    Con.Close();
                    populate();
                    Reset();    
                }catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
                
            }
        }

        private void CatCbSearchCb_SelectionChangeCommitted(object sender, EventArgs e)
        {
            Filter();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            populate();
            CatCbSearchCb.SelectedIndex = -1;
        }
        private void Reset()
        {
            BnameTb.Text = "";
            BAuthorTb.Text = "";
            BcatCbo.SelectedIndex = -1;
            PriceTb.Text = "";
            QtyTb.Text = "";

        }
        private void ResetBtn_Click(object sender, EventArgs e)
        {
            Reset();
        }
        int key = 0;

        private void BookDgv_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            BnameTb.Text = BookDgv.SelectedRows[0].Cells[1].Value.ToString();
            BAuthorTb.Text= BookDgv.SelectedRows[0].Cells[2].Value.ToString();
            BcatCbo.SelectedItem = BookDgv.SelectedRows[0].Cells[3].Value.ToString();
            QtyTb.Text = BookDgv.SelectedRows[0].Cells[4].Value.ToString();
            PriceTb.Text = BookDgv.SelectedRows[0].Cells[5].Value.ToString();
            if(BnameTb.Text=="")
            {
                key = 0;
            }else
            {
                key = Convert.ToInt32(BookDgv.SelectedRows[0].Cells[1].Value.ToString());

            }
        }

        private void DeleteBtn_Click(object sender, EventArgs e)
        {
            if (key ==0)
            {
                MessageBox.Show("Điền thiếu thông tin");
            }
            else
            {
                try
                {
                    Con.Open();
                    string query = " delete from BookTbl where BId =" + key + ";";
                     SqlCommand cmd = new SqlCommand(query, Con);
                    cmd.ExecuteNonQuery();
                    MessageBox.Show("Sách đã được xóa thành công");
                    Con.Close();
                    populate();
                    Reset();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }

            }
        }

        private void EditBtn_Click(object sender, EventArgs e)
        {
            if (BnameTb.Text == "" || BAuthorTb.Text == "" || QtyTb.Text == "" || PriceTb.Text == "" || BcatCbo.SelectedIndex == -1)
            {
                MessageBox.Show("Điền thiếu thông tin");
            }
            else
            {
                try
                {
                    Con.Open();
                    string query = "update BookTbl set BTitle='" + BnameTb.Text + "',Bauthor='" + BAuthorTb.Text + "',BCat='" + BcatCbo.SelectedItem.ToString() + "',Bqty=" + QtyTb.Text + ",BPrice=" + PriceTb.Text + "where BId="+key+";";
                    SqlCommand cmd = new SqlCommand(query, Con);
                    cmd.ExecuteNonQuery();
                    MessageBox.Show("Sách đã được sửa thành công");
                    Con.Close();
                    populate();
                    Reset();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }

            }

        }
    }
}

﻿namespace BookStoreApp
{
    partial class Books
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.Label label4;
            System.Windows.Forms.Label label3;
            System.Windows.Forms.Label label2;
            System.Windows.Forms.Label label5;
            System.Windows.Forms.Label label6;
            System.Windows.Forms.Label label7;
            System.Windows.Forms.Label label8;
            System.Windows.Forms.Label label1;
            System.Windows.Forms.Label label9;
            System.Windows.Forms.Label label10;
            System.Windows.Forms.Label label11;
            this.panel2 = new System.Windows.Forms.Panel();
            this.BookDgv = new System.Windows.Forms.DataGridView();
            this.CatCbSearchCb = new System.Windows.Forms.ComboBox();
            this.BcatCbo = new System.Windows.Forms.ComboBox();
            this.DeleteBtn = new System.Windows.Forms.Button();
            this.EditBtn = new System.Windows.Forms.Button();
            this.SaveBtn = new System.Windows.Forms.Button();
            this.PriceTb = new System.Windows.Forms.TextBox();
            this.QtyTb = new System.Windows.Forms.TextBox();
            this.BAuthorTb = new System.Windows.Forms.TextBox();
            this.BnameTb = new System.Windows.Forms.TextBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel6 = new System.Windows.Forms.Panel();
            this.panel5 = new System.Windows.Forms.Panel();
            this.panel4 = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.ResetBtn = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            label4 = new System.Windows.Forms.Label();
            label3 = new System.Windows.Forms.Label();
            label2 = new System.Windows.Forms.Label();
            label5 = new System.Windows.Forms.Label();
            label6 = new System.Windows.Forms.Label();
            label7 = new System.Windows.Forms.Label();
            label8 = new System.Windows.Forms.Label();
            label1 = new System.Windows.Forms.Label();
            label9 = new System.Windows.Forms.Label();
            label10 = new System.Windows.Forms.Label();
            label11 = new System.Windows.Forms.Label();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.BookDgv)).BeginInit();
            this.panel1.SuspendLayout();
            this.panel6.SuspendLayout();
            this.panel5.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panel3.SuspendLayout();
            this.SuspendLayout();
            // 
            // label4
            // 
            label4.AutoSize = true;
            label4.Font = new System.Drawing.Font("Times New Roman", 15.75F);
            label4.Location = new System.Drawing.Point(237, 101);
            label4.Name = "label4";
            label4.Size = new System.Drawing.Size(70, 23);
            label4.TabIndex = 1;
            label4.Text = "Tác giả";
            // 
            // label3
            // 
            label3.AutoSize = true;
            label3.Font = new System.Drawing.Font("Times New Roman", 15.75F);
            label3.Location = new System.Drawing.Point(8, 101);
            label3.Name = "label3";
            label3.Size = new System.Drawing.Size(85, 23);
            label3.TabIndex = 1;
            label3.Text = "Tên Sách";
            // 
            // label2
            // 
            label2.AutoSize = true;
            label2.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            label2.Location = new System.Drawing.Point(1124, 0);
            label2.Name = "label2";
            label2.Size = new System.Drawing.Size(22, 22);
            label2.TabIndex = 1;
            label2.Text = "X";
            label2.Click += new System.EventHandler(this.label2_Click);
            // 
            // label5
            // 
            label5.AutoSize = true;
            label5.Font = new System.Drawing.Font("Times New Roman", 15.75F);
            label5.ForeColor = System.Drawing.SystemColors.Control;
            label5.Location = new System.Drawing.Point(39, 6);
            label5.Name = "label5";
            label5.Size = new System.Drawing.Size(49, 23);
            label5.TabIndex = 1;
            label5.Text = "Sách";
            // 
            // label6
            // 
            label6.AutoSize = true;
            label6.BackColor = System.Drawing.Color.Black;
            label6.Font = new System.Drawing.Font("Times New Roman", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            label6.ForeColor = System.Drawing.Color.White;
            label6.Location = new System.Drawing.Point(39, 13);
            label6.Name = "label6";
            label6.Size = new System.Drawing.Size(109, 23);
            label6.TabIndex = 1;
            label6.Text = "Người dùng";
            // 
            // label7
            // 
            label7.AutoSize = true;
            label7.BackColor = System.Drawing.Color.Black;
            label7.Font = new System.Drawing.Font("Times New Roman", 15.75F);
            label7.ForeColor = System.Drawing.Color.White;
            label7.Location = new System.Drawing.Point(39, 11);
            label7.Name = "label7";
            label7.Size = new System.Drawing.Size(99, 23);
            label7.TabIndex = 1;
            label7.Text = "Dashboard";
            // 
            // label8
            // 
            label8.AutoSize = true;
            label8.Font = new System.Drawing.Font("Times New Roman", 15.75F);
            label8.ForeColor = System.Drawing.Color.White;
            label8.Location = new System.Drawing.Point(39, 11);
            label8.Name = "label8";
            label8.Size = new System.Drawing.Size(71, 23);
            label8.TabIndex = 1;
            label8.Text = "Logout";
            // 
            // label1
            // 
            label1.AutoSize = true;
            label1.Font = new System.Drawing.Font("Times New Roman", 15.75F);
            label1.Location = new System.Drawing.Point(468, 101);
            label1.Name = "label1";
            label1.Size = new System.Drawing.Size(78, 23);
            label1.TabIndex = 1;
            label1.Text = "Thể loại";
            // 
            // label9
            // 
            label9.AutoSize = true;
            label9.Font = new System.Drawing.Font("Times New Roman", 15.75F);
            label9.Location = new System.Drawing.Point(697, 101);
            label9.Name = "label9";
            label9.Size = new System.Drawing.Size(85, 23);
            label9.TabIndex = 1;
            label9.Text = "Số lượng";
            // 
            // label10
            // 
            label10.AutoSize = true;
            label10.Font = new System.Drawing.Font("Times New Roman", 15.75F);
            label10.Location = new System.Drawing.Point(921, 101);
            label10.Name = "label10";
            label10.Size = new System.Drawing.Size(76, 23);
            label10.TabIndex = 1;
            label10.Text = "Giá tiền";
            // 
            // label11
            // 
            label11.AutoSize = true;
            label11.Font = new System.Drawing.Font("Times New Roman", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            label11.Location = new System.Drawing.Point(530, 260);
            label11.Name = "label11";
            label11.Size = new System.Drawing.Size(101, 27);
            label11.TabIndex = 7;
            label11.Text = "List Sách";
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.SystemColors.Control;
            this.panel2.Controls.Add(label11);
            this.panel2.Controls.Add(this.BookDgv);
            this.panel2.Controls.Add(this.CatCbSearchCb);
            this.panel2.Controls.Add(this.BcatCbo);
            this.panel2.Controls.Add(this.button1);
            this.panel2.Controls.Add(this.ResetBtn);
            this.panel2.Controls.Add(this.DeleteBtn);
            this.panel2.Controls.Add(this.EditBtn);
            this.panel2.Controls.Add(this.SaveBtn);
            this.panel2.Controls.Add(this.PriceTb);
            this.panel2.Controls.Add(this.QtyTb);
            this.panel2.Controls.Add(this.BAuthorTb);
            this.panel2.Controls.Add(label10);
            this.panel2.Controls.Add(label9);
            this.panel2.Controls.Add(this.BnameTb);
            this.panel2.Controls.Add(label1);
            this.panel2.Controls.Add(label4);
            this.panel2.Controls.Add(label3);
            this.panel2.Controls.Add(label2);
            this.panel2.Location = new System.Drawing.Point(217, 12);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(1156, 705);
            this.panel2.TabIndex = 1;
            // 
            // BookDgv
            // 
            this.BookDgv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.BookDgv.Location = new System.Drawing.Point(27, 331);
            this.BookDgv.Name = "BookDgv";
            this.BookDgv.Size = new System.Drawing.Size(1119, 361);
            this.BookDgv.TabIndex = 6;
            this.BookDgv.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.BookDgv_CellContentClick);
            // 
            // CatCbSearchCb
            // 
            this.CatCbSearchCb.Font = new System.Drawing.Font("Times New Roman", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CatCbSearchCb.FormattingEnabled = true;
            this.CatCbSearchCb.Items.AddRange(new object[] {
            "Lập trình",
            "Toán",
            "Tiểu thuyết",
            "Sinh học",
            "Vật lý"});
            this.CatCbSearchCb.Location = new System.Drawing.Point(492, 290);
            this.CatCbSearchCb.Name = "CatCbSearchCb";
            this.CatCbSearchCb.Size = new System.Drawing.Size(174, 29);
            this.CatCbSearchCb.TabIndex = 5;
            this.CatCbSearchCb.Text = "Lọc theo thể loại";
            this.CatCbSearchCb.SelectionChangeCommitted += new System.EventHandler(this.CatCbSearchCb_SelectionChangeCommitted);
            // 
            // BcatCbo
            // 
            this.BcatCbo.Font = new System.Drawing.Font("Times New Roman", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BcatCbo.FormattingEnabled = true;
            this.BcatCbo.Items.AddRange(new object[] {
            "Lập trình",
            "Toán",
            "Tiểu thuyết",
            "Sinh học",
            "Vật lý"});
            this.BcatCbo.Location = new System.Drawing.Point(472, 138);
            this.BcatCbo.Name = "BcatCbo";
            this.BcatCbo.Size = new System.Drawing.Size(221, 29);
            this.BcatCbo.TabIndex = 5;
            this.BcatCbo.Text = "Chọn thể loại";
            // 
            // DeleteBtn
            // 
            this.DeleteBtn.Font = new System.Drawing.Font("Times New Roman", 12F);
            this.DeleteBtn.Location = new System.Drawing.Point(610, 185);
            this.DeleteBtn.Name = "DeleteBtn";
            this.DeleteBtn.Size = new System.Drawing.Size(150, 56);
            this.DeleteBtn.TabIndex = 4;
            this.DeleteBtn.Text = "Xóa";
            this.DeleteBtn.UseVisualStyleBackColor = true;
            this.DeleteBtn.Click += new System.EventHandler(this.DeleteBtn_Click);
            // 
            // EditBtn
            // 
            this.EditBtn.Font = new System.Drawing.Font("Times New Roman", 12F);
            this.EditBtn.Location = new System.Drawing.Point(454, 185);
            this.EditBtn.Name = "EditBtn";
            this.EditBtn.Size = new System.Drawing.Size(150, 56);
            this.EditBtn.TabIndex = 4;
            this.EditBtn.Text = "Sửa";
            this.EditBtn.UseVisualStyleBackColor = true;
            this.EditBtn.Click += new System.EventHandler(this.EditBtn_Click);
            // 
            // SaveBtn
            // 
            this.SaveBtn.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SaveBtn.Location = new System.Drawing.Point(298, 185);
            this.SaveBtn.Name = "SaveBtn";
            this.SaveBtn.Size = new System.Drawing.Size(150, 56);
            this.SaveBtn.TabIndex = 4;
            this.SaveBtn.Text = "Lưu";
            this.SaveBtn.UseVisualStyleBackColor = true;
            this.SaveBtn.Click += new System.EventHandler(this.SaveBtn_Click);
            // 
            // PriceTb
            // 
            this.PriceTb.Font = new System.Drawing.Font("Century Gothic", 14.25F);
            this.PriceTb.Location = new System.Drawing.Point(925, 138);
            this.PriceTb.Name = "PriceTb";
            this.PriceTb.Size = new System.Drawing.Size(221, 31);
            this.PriceTb.TabIndex = 3;
            // 
            // QtyTb
            // 
            this.QtyTb.Font = new System.Drawing.Font("Century Gothic", 14.25F);
            this.QtyTb.Location = new System.Drawing.Point(701, 138);
            this.QtyTb.Name = "QtyTb";
            this.QtyTb.Size = new System.Drawing.Size(221, 31);
            this.QtyTb.TabIndex = 3;
            // 
            // BAuthorTb
            // 
            this.BAuthorTb.Font = new System.Drawing.Font("Century Gothic", 14.25F);
            this.BAuthorTb.Location = new System.Drawing.Point(241, 138);
            this.BAuthorTb.Name = "BAuthorTb";
            this.BAuthorTb.Size = new System.Drawing.Size(221, 31);
            this.BAuthorTb.TabIndex = 3;
            // 
            // BnameTb
            // 
            this.BnameTb.Font = new System.Drawing.Font("Century Gothic", 14.25F);
            this.BnameTb.Location = new System.Drawing.Point(8, 138);
            this.BnameTb.Name = "BnameTb";
            this.BnameTb.Size = new System.Drawing.Size(221, 31);
            this.BnameTb.TabIndex = 3;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.SystemColors.ControlText;
            this.panel1.Controls.Add(this.panel6);
            this.panel1.Controls.Add(this.panel5);
            this.panel1.Controls.Add(this.panel4);
            this.panel1.Controls.Add(this.panel3);
            this.panel1.Location = new System.Drawing.Point(12, 12);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(208, 705);
            this.panel1.TabIndex = 2;
            // 
            // panel6
            // 
            this.panel6.BackColor = System.Drawing.Color.Transparent;
            this.panel6.Controls.Add(label8);
            this.panel6.Location = new System.Drawing.Point(9, 351);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(191, 46);
            this.panel6.TabIndex = 0;
            // 
            // panel5
            // 
            this.panel5.BackColor = System.Drawing.Color.Transparent;
            this.panel5.Controls.Add(label7);
            this.panel5.Location = new System.Drawing.Point(9, 299);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(191, 46);
            this.panel5.TabIndex = 0;
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.Transparent;
            this.panel4.Controls.Add(label6);
            this.panel4.Location = new System.Drawing.Point(9, 247);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(191, 46);
            this.panel4.TabIndex = 0;
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.Green;
            this.panel3.Controls.Add(label5);
            this.panel3.Location = new System.Drawing.Point(9, 195);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(191, 46);
            this.panel3.TabIndex = 0;
            // 
            // ResetBtn
            // 
            this.ResetBtn.Font = new System.Drawing.Font("Times New Roman", 12F);
            this.ResetBtn.Location = new System.Drawing.Point(766, 185);
            this.ResetBtn.Name = "ResetBtn";
            this.ResetBtn.Size = new System.Drawing.Size(150, 56);
            this.ResetBtn.TabIndex = 4;
            this.ResetBtn.Text = "Đặt lại";
            this.ResetBtn.UseVisualStyleBackColor = true;
            this.ResetBtn.Click += new System.EventHandler(this.ResetBtn_Click);
            // 
            // button1
            // 
            this.button1.Font = new System.Drawing.Font("Times New Roman", 12F);
            this.button1.Location = new System.Drawing.Point(681, 290);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(150, 29);
            this.button1.TabIndex = 4;
            this.button1.Text = "Cập nhật";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // Books
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Indigo;
            this.ClientSize = new System.Drawing.Size(1386, 729);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Books";
            this.Text = "Books";
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.BookDgv)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel6.ResumeLayout(false);
            this.panel6.PerformLayout();
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button SaveBtn;
        private System.Windows.Forms.TextBox BAuthorTb;
        private System.Windows.Forms.TextBox BnameTb;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.DataGridView BookDgv;
        private System.Windows.Forms.ComboBox BcatCbo;
        private System.Windows.Forms.Button DeleteBtn;
        private System.Windows.Forms.Button EditBtn;
        private System.Windows.Forms.TextBox PriceTb;
        private System.Windows.Forms.TextBox QtyTb;
        private System.Windows.Forms.ComboBox CatCbSearchCb;
        private System.Windows.Forms.Button ResetBtn;
        private System.Windows.Forms.Button button1;
    }
}
﻿namespace BookStoreApp
{
    partial class Users
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.Label label11;
            System.Windows.Forms.Label label10;
            System.Windows.Forms.Label label9;
            System.Windows.Forms.Label label8;
            System.Windows.Forms.Label label7;
            System.Windows.Forms.Label label6;
            System.Windows.Forms.Label label5;
            System.Windows.Forms.Label label4;
            System.Windows.Forms.Label label3;
            System.Windows.Forms.Label label2;
            this.button3 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.PassTb = new System.Windows.Forms.TextBox();
            this.AddTb = new System.Windows.Forms.TextBox();
            this.panel6 = new System.Windows.Forms.Panel();
            this.panel5 = new System.Windows.Forms.Panel();
            this.PhoneTb = new System.Windows.Forms.TextBox();
            this.panel4 = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.SaveBtn = new System.Windows.Forms.Button();
            this.panel2 = new System.Windows.Forms.Panel();
            this.UnameTb = new System.Windows.Forms.TextBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.UserDgv = new System.Windows.Forms.DataGridView();
            label11 = new System.Windows.Forms.Label();
            label10 = new System.Windows.Forms.Label();
            label9 = new System.Windows.Forms.Label();
            label8 = new System.Windows.Forms.Label();
            label7 = new System.Windows.Forms.Label();
            label6 = new System.Windows.Forms.Label();
            label5 = new System.Windows.Forms.Label();
            label4 = new System.Windows.Forms.Label();
            label3 = new System.Windows.Forms.Label();
            label2 = new System.Windows.Forms.Label();
            this.panel6.SuspendLayout();
            this.panel5.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.UserDgv)).BeginInit();
            this.SuspendLayout();
            // 
            // label11
            // 
            label11.AutoSize = true;
            label11.Font = new System.Drawing.Font("Times New Roman", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            label11.Location = new System.Drawing.Point(472, 285);
            label11.Name = "label11";
            label11.Size = new System.Drawing.Size(240, 27);
            label11.TabIndex = 7;
            label11.Text = "Danh Sách Người Dùng";
            // 
            // button3
            // 
            this.button3.Font = new System.Drawing.Font("Times New Roman", 12F);
            this.button3.Location = new System.Drawing.Point(610, 185);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(150, 56);
            this.button3.TabIndex = 4;
            this.button3.Text = "Xóa";
            this.button3.UseVisualStyleBackColor = true;
            // 
            // button2
            // 
            this.button2.Font = new System.Drawing.Font("Times New Roman", 12F);
            this.button2.Location = new System.Drawing.Point(454, 185);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(150, 56);
            this.button2.TabIndex = 4;
            this.button2.Text = "Sửa";
            this.button2.UseVisualStyleBackColor = true;
            // 
            // PassTb
            // 
            this.PassTb.Font = new System.Drawing.Font("Century Gothic", 14.25F);
            this.PassTb.Location = new System.Drawing.Point(695, 138);
            this.PassTb.Name = "PassTb";
            this.PassTb.Size = new System.Drawing.Size(195, 31);
            this.PassTb.TabIndex = 3;
            // 
            // AddTb
            // 
            this.AddTb.Font = new System.Drawing.Font("Century Gothic", 14.25F);
            this.AddTb.Location = new System.Drawing.Point(468, 138);
            this.AddTb.Name = "AddTb";
            this.AddTb.Size = new System.Drawing.Size(221, 31);
            this.AddTb.TabIndex = 3;
            this.AddTb.TextChanged += new System.EventHandler(this.textBox4_TextChanged);
            // 
            // label10
            // 
            label10.AutoSize = true;
            label10.Font = new System.Drawing.Font("Times New Roman", 15.75F);
            label10.Location = new System.Drawing.Point(691, 101);
            label10.Name = "label10";
            label10.Size = new System.Drawing.Size(88, 23);
            label10.TabIndex = 1;
            label10.Text = "Mật khẩu";
            label10.Click += new System.EventHandler(this.label10_Click);
            // 
            // label9
            // 
            label9.AutoSize = true;
            label9.Font = new System.Drawing.Font("Times New Roman", 15.75F);
            label9.Location = new System.Drawing.Point(464, 101);
            label9.Name = "label9";
            label9.Size = new System.Drawing.Size(70, 23);
            label9.TabIndex = 1;
            label9.Text = "Địa chỉ";
            // 
            // panel6
            // 
            this.panel6.BackColor = System.Drawing.Color.Transparent;
            this.panel6.Controls.Add(label8);
            this.panel6.Location = new System.Drawing.Point(9, 351);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(191, 46);
            this.panel6.TabIndex = 0;
            // 
            // label8
            // 
            label8.AutoSize = true;
            label8.Font = new System.Drawing.Font("Times New Roman", 15.75F);
            label8.ForeColor = System.Drawing.Color.White;
            label8.Location = new System.Drawing.Point(39, 11);
            label8.Name = "label8";
            label8.Size = new System.Drawing.Size(71, 23);
            label8.TabIndex = 1;
            label8.Text = "Logout";
            // 
            // panel5
            // 
            this.panel5.BackColor = System.Drawing.Color.Transparent;
            this.panel5.Controls.Add(label7);
            this.panel5.Location = new System.Drawing.Point(9, 299);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(191, 46);
            this.panel5.TabIndex = 0;
            // 
            // label7
            // 
            label7.AutoSize = true;
            label7.Font = new System.Drawing.Font("Times New Roman", 15.75F);
            label7.ForeColor = System.Drawing.Color.White;
            label7.Location = new System.Drawing.Point(39, 11);
            label7.Name = "label7";
            label7.Size = new System.Drawing.Size(99, 23);
            label7.TabIndex = 1;
            label7.Text = "Dashboard";
            // 
            // PhoneTb
            // 
            this.PhoneTb.Font = new System.Drawing.Font("Century Gothic", 14.25F);
            this.PhoneTb.Location = new System.Drawing.Point(241, 138);
            this.PhoneTb.Name = "PhoneTb";
            this.PhoneTb.Size = new System.Drawing.Size(221, 31);
            this.PhoneTb.TabIndex = 3;
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.Green;
            this.panel4.Controls.Add(label6);
            this.panel4.Location = new System.Drawing.Point(9, 247);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(191, 46);
            this.panel4.TabIndex = 0;
            // 
            // label6
            // 
            label6.AutoSize = true;
            label6.Font = new System.Drawing.Font("Times New Roman", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            label6.Location = new System.Drawing.Point(39, 13);
            label6.Name = "label6";
            label6.Size = new System.Drawing.Size(109, 23);
            label6.TabIndex = 1;
            label6.Text = "Người dùng";
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.Transparent;
            this.panel3.Controls.Add(label5);
            this.panel3.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.panel3.Location = new System.Drawing.Point(9, 195);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(191, 46);
            this.panel3.TabIndex = 0;
            // 
            // label5
            // 
            label5.AutoSize = true;
            label5.Font = new System.Drawing.Font("Times New Roman", 15.75F);
            label5.ForeColor = System.Drawing.SystemColors.ButtonFace;
            label5.Location = new System.Drawing.Point(39, 12);
            label5.Name = "label5";
            label5.Size = new System.Drawing.Size(49, 23);
            label5.TabIndex = 1;
            label5.Text = "Sách";
            label5.Click += new System.EventHandler(this.label5_Click);
            // 
            // SaveBtn
            // 
            this.SaveBtn.Font = new System.Drawing.Font("Times New Roman", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SaveBtn.Location = new System.Drawing.Point(298, 185);
            this.SaveBtn.Name = "SaveBtn";
            this.SaveBtn.Size = new System.Drawing.Size(150, 56);
            this.SaveBtn.TabIndex = 4;
            this.SaveBtn.Text = "Lưu";
            this.SaveBtn.UseVisualStyleBackColor = true;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.SystemColors.Control;
            this.panel2.Controls.Add(this.UserDgv);
            this.panel2.Controls.Add(label11);
            this.panel2.Controls.Add(this.button3);
            this.panel2.Controls.Add(this.button2);
            this.panel2.Controls.Add(this.SaveBtn);
            this.panel2.Controls.Add(this.PassTb);
            this.panel2.Controls.Add(this.AddTb);
            this.panel2.Controls.Add(this.PhoneTb);
            this.panel2.Controls.Add(label10);
            this.panel2.Controls.Add(label9);
            this.panel2.Controls.Add(this.UnameTb);
            this.panel2.Controls.Add(label4);
            this.panel2.Controls.Add(label3);
            this.panel2.Controls.Add(label2);
            this.panel2.Location = new System.Drawing.Point(218, 12);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(1156, 705);
            this.panel2.TabIndex = 3;
            // 
            // UnameTb
            // 
            this.UnameTb.Font = new System.Drawing.Font("Century Gothic", 14.25F);
            this.UnameTb.Location = new System.Drawing.Point(8, 138);
            this.UnameTb.Name = "UnameTb";
            this.UnameTb.Size = new System.Drawing.Size(221, 31);
            this.UnameTb.TabIndex = 3;
            // 
            // label4
            // 
            label4.AutoSize = true;
            label4.Font = new System.Drawing.Font("Times New Roman", 15.75F);
            label4.Location = new System.Drawing.Point(237, 101);
            label4.Name = "label4";
            label4.Size = new System.Drawing.Size(130, 23);
            label4.TabIndex = 1;
            label4.Text = "Số Điện Thoại";
            // 
            // label3
            // 
            label3.AutoSize = true;
            label3.Font = new System.Drawing.Font("Times New Roman", 15.75F);
            label3.Location = new System.Drawing.Point(8, 101);
            label3.Name = "label3";
            label3.Size = new System.Drawing.Size(150, 23);
            label3.TabIndex = 1;
            label3.Text = "Tên Người Dùng";
            // 
            // label2
            // 
            label2.AutoSize = true;
            label2.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            label2.Location = new System.Drawing.Point(1058, 0);
            label2.Name = "label2";
            label2.Size = new System.Drawing.Size(22, 22);
            label2.TabIndex = 1;
            label2.Text = "X";
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.SystemColors.ControlText;
            this.panel1.Controls.Add(this.panel6);
            this.panel1.Controls.Add(this.panel5);
            this.panel1.Controls.Add(this.panel4);
            this.panel1.Controls.Add(this.panel3);
            this.panel1.Location = new System.Drawing.Point(12, 12);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(208, 705);
            this.panel1.TabIndex = 4;
            // 
            // UserDgv
            // 
            this.UserDgv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.UserDgv.Location = new System.Drawing.Point(34, 329);
            this.UserDgv.Name = "UserDgv";
            this.UserDgv.Size = new System.Drawing.Size(1119, 361);
            this.UserDgv.TabIndex = 8;
            // 
            // Users
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Indigo;
            this.ClientSize = new System.Drawing.Size(1386, 729);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Users";
            this.Text = "Users";
            this.panel6.ResumeLayout(false);
            this.panel6.PerformLayout();
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.UserDgv)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.TextBox PassTb;
        private System.Windows.Forms.TextBox AddTb;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.TextBox PhoneTb;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Button SaveBtn;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.TextBox UnameTb;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.DataGridView UserDgv;
    }
}